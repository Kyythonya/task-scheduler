﻿using Scheduler.DataAccess;
using Scheduler.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Scheduler.Service;
using System.Windows.Documents;
using System;
using FluentScheduler;
using System.Windows.Threading;

namespace SchedulerWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private User _currentUser;
        private FileService _fileService;
        private Mailer _mailer;

        public MainWindow()
        {
            InitializeComponent();

            using (var repository = new Repository())
            {
                var users = repository.Select<User>();
                _currentUser = new List<User>(users)[0];
            }

            _fileService = new FileService();
            _mailer = new Mailer();
            
            fromMail.Text = _currentUser.Email;

            periodicityComboBox.ItemsSource = Enum.GetNames(typeof(PeriodicityType));
            periodicityComboBox.SelectedItem = "Day";

            methodsComboBox.ItemsSource = new string[] { "Send" };
            methodsComboBox.SelectedItem = "Send"; 
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
            base.OnClosing(e);

            taskBarIcon.Visibility = Visibility.Visible;
        }

        private void OpenCommandExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Show();

            taskBarIcon.Visibility = Visibility.Collapsed;
        }

        private void Load(bool isLoading)
        {
            if (isLoading)
            {
                progressBarResult.Visibility = Visibility.Visible;
                progressResultText.Text = "Loading...";
            }
            else
            {
                progressBarResult.Visibility = Visibility.Collapsed;
                progressResultText.Text = "Done.";
            }
        }
        
        private void CheckPasswordButtonClick(object sender, RoutedEventArgs e)
        {
            if (passwordMail.Password == "")
            {
                MessageBox.Show("Enter the password!");
                return;
            }
            else if (!SecurityHasher.VerifyPassword(passwordMail.Password, _currentUser.Password))
            {
                MessageBox.Show("Your login or password isn't correct! Try again!");
                return;
            }

            passwordCheckDialog.IsOpen = false;
        }

        private async void SendMail()
        {
            Load(true);

            var result = await _mailer.SendMail(toMail.Text, fromMail.Text, passwordMail.Password, subjectMail.Text, new TextRange(bodyMail.Document.ContentStart, bodyMail.Document.ContentEnd).Text);

            Load(false);

            MessageBox.Show(result);
        }

        private async void DownloadFileButtonClick(object sender, RoutedEventArgs e)
        {
            if (fileUrl.Text == "" || filePath.Text == "")
            {
                MessageBox.Show("Complete the form!");
                return;
            }

            Load(true);

            var result = await _fileService.Download(fileUrl.Text, filePath.Text);

            Load(false);

            MessageBox.Show(result);
        }

        private void MoveFileButtonClick(object sender, RoutedEventArgs e)
        {
            if (fileToPath.Text == "" || fileFromPath.Text == "")
            {
                MessageBox.Show("Complete the form!");
                return;
            }

            var result = _fileService.Move(fileFromPath.Text, fileToPath.Text);
            MessageBox.Show(result);
        }

        private async void AddEventButtonClick(object sender, RoutedEventArgs e)
        {
            if (eventName.Text == "" || startDatePicker.Text == "" || startTime.Text == "" || endDatePicker.Text == "" || endTime.Text == "")
            {
                MessageBox.Show("Complete the form!");
                return;
            }
            else if (!TimeSpan.TryParse(startTime.Text, out TimeSpan timeStart) || !TimeSpan.TryParse(endTime.Text, out TimeSpan timeEnd))
            {
                MessageBox.Show("Incorrect time format!");
                return;
            }
            else if (startDatePicker.DisplayDate.AddHours(timeStart.Hours).AddMinutes(timeStart.Minutes) >= endDatePicker.DisplayDate.AddHours(timeEnd.Hours).AddMinutes(timeEnd.Minutes))
            {
                MessageBox.Show("End date can't be earlier or the same with start date!");
                return;
            }

            var newEvent = new Event
            {
                Name = eventName.Text,
                StartTime = startDatePicker.DisplayDate,
                EndTime = startDatePicker.DisplayDate,
                Periodicity = (PeriodicityType)Enum.Parse(typeof(PeriodicityType), periodicityComboBox.Text)
            };

            using (var repository = new Repository())
            {
                var existedEvent = new List<Event>(repository.Select<Event>()).Find(@event => @event.Name == newEvent.Name && @event.Periodicity == newEvent.Periodicity
                                                                                        && @event.StartTime == newEvent.StartTime && @event.EndTime == newEvent.EndTime);
                if (existedEvent != null)
                {
                    MessageBox.Show("Event with such info already exists!");
                    return;
                }
            }

            using (var context = new DataContext())
            {
                context.Users.Attach(_currentUser);
                _currentUser.Events.Add(newEvent);

                await context.SaveChangesAsync();
            }

            invokeSendMethodButton.IsEnabled = true;
        }

        private void InvokeSendMethodButtonClick(object sender, RoutedEventArgs e)
        {
            if (fromMail.Text == "" || toMail.Text == "" || subjectMail.Text == "" || new TextRange(bodyMail.Document.ContentStart, bodyMail.Document.ContentEnd).Text == "")
            {
                MessageBox.Show("Complete the form!");
                return;
            }

            invokeSendMethodButton.IsEnabled = false;

            var timeStart = TimeSpan.Parse(startTime.Text);
            var timeEnd = TimeSpan.Parse(endTime.Text);
            
            passwordCheckDialog.IsOpen = true;

            JobManager.InitializeWithoutStarting(new Registry());

            string currentJobName = "";
            switch (periodicityComboBox.Text)
            {
                case "Day":
                    currentJobName = "SendMailsDaily";
                    JobManager.AddJob(() => Dispatcher.BeginInvoke(new Action(SendMail), DispatcherPriority.Background), (schedule) => schedule.WithName(currentJobName).ToRunEvery(1).Days().At(timeStart.Hours, timeStart.Minutes));
                    break;
                case "Week":
                    currentJobName = "SendMailsWeekly";
                    JobManager.AddJob(() => Dispatcher.BeginInvoke(new Action(SendMail), DispatcherPriority.Background), (schedule) => schedule.WithName(currentJobName).ToRunEvery(1).Weeks().On(DayOfWeek.Monday).At(timeStart.Hours, timeStart.Minutes));
                    break;
                case "Month":
                    currentJobName = "SendMailsEveryMonth";
                    JobManager.AddJob(() => Dispatcher.BeginInvoke(new Action(SendMail), DispatcherPriority.Background), (schedule) => schedule.WithName(currentJobName).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Monday).At(timeStart.Hours, timeStart.Minutes));
                    break;
                case "Year":
                    currentJobName = "SendMailsEveryYear";
                    JobManager.AddJob(() => Dispatcher.BeginInvoke(new Action(SendMail), DispatcherPriority.Background), (schedule) => schedule.WithName(currentJobName).ToRunEvery(1).Years().On(1).At(timeStart.Hours, timeStart.Minutes));
                    break;
            }

            if (startDatePicker.DisplayDate == DateTime.Now.Date)
            {
                JobManager.Start();
            }

            if (endDatePicker.DisplayDate.AddHours(timeEnd.Hours).AddMinutes(timeEnd.Minutes) == DateTime.Now.Date)
            {
                JobManager.Stop();
                JobManager.RemoveJob(currentJobName);
            }
        }
    }
}
