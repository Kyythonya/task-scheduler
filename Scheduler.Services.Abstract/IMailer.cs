﻿using System.Threading.Tasks;

namespace Scheduler.Services.Abstract
{
    public interface IMailer
    {
        Task<string> SendMail(string toMail, string fromMail, string fromMailPassword, string mailSubject, string mailText);
    }
}
