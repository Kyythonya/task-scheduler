﻿namespace Scheduler.Models
{
    public enum PeriodicityType
    {
        Day, Week, Month, Year 
    }
}
