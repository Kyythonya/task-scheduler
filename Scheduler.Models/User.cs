﻿using System;
using System.Collections.Generic;

namespace Scheduler.Models
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string Email { get; set; }

        public string Password { get; set; }

        public IList<Event> Events { get; set; } = new List<Event>();
    }
}
